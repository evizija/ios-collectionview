import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var label: UILabel!
    @IBOutlet var countLabel: UILabel!
    
    private var collectionViewLayout: LGHorizontalLinearFlowLayout!
    
    var stringArray: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"]
    var selectedCell: Int! = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.pagingEnabled = false
        
        selectedCell = 0
        
        self.collectionViewLayout = LGHorizontalLinearFlowLayout
            .configureLayout(self.collectionView, itemSize: CGSizeMake(90, 90), minimumLineSpacing: 10)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stringArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("collectionCell", forIndexPath: indexPath)
            as! collectionViewCell
        
        cell.label.text = stringArray[indexPath.item]
        
        cell.layer.shouldRasterize = true;
        cell.layer.rasterizationScale = UIScreen.mainScreen().scale
        
        if(selectedCell != nil){
            if(indexPath.item == selectedCell){
                cell.image.image = UIImage(named: "FullCircle")!
                cell.label.textColor = UIColor.whiteColor()
            }
            else{
                cell.image.image = UIImage(named: "EmptyCircle")!
                cell.label.textColor = UIColor.blackColor()
            }
        }
        
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        findCenterIndex(scrollView)
    }
    
    func findCenterIndex(scrollView: UIScrollView) {
        let collectionOrigin = collectionView!.bounds.origin
        let collectionWidth = collectionView!.bounds.width
        var centerPoint: CGPoint!
        var newX: CGFloat!
        if collectionOrigin.x > 0 {
            newX = collectionOrigin.x + collectionWidth / 2
            centerPoint = CGPoint(x: newX, y: collectionOrigin.y)
        } else {
            newX = collectionWidth / 2
            centerPoint = CGPoint(x: newX, y: collectionOrigin.y)
        }
        
        let index = collectionView!.indexPathForItemAtPoint(centerPoint)
        let cell = collectionView!.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? collectionViewCell
        
        if(index != nil){
            for cell in self.collectionView.visibleCells()   {
                let currentCell = cell as! collectionViewCell
                currentCell.image.image = UIImage(named: "EmptyCircle")!
                currentCell.label.textColor = UIColor.blackColor()
            }
            
            let cell = collectionView.cellForItemAtIndexPath(index!) as? collectionViewCell
            if(cell != nil){
                cell!.image.image = UIImage(named: "FullCircle")!
                selectedCell = collectionView.indexPathForCell(cell!)?.item
                self.countLabel.text = stringArray[selectedCell!]
            }
        }
        else if(cell != nil){
            let actualPosition = scrollView.panGestureRecognizer.translationInView(scrollView.superview)
            for cellView in self.collectionView.visibleCells()   {
                let currentCell = cellView as? collectionViewCell
                    currentCell!.image.image = UIImage(named: "EmptyCircle")!
                    currentCell!.label.textColor = UIColor.blackColor()
                
                if(currentCell == cell! && (selectedCell == 0 || selectedCell == 1) && actualPosition.x > 0){
                    cell!.image.image = UIImage(named: "FullCircle")!
                    selectedCell = collectionView.indexPathForCell(cell!)?.item
                    self.countLabel.text = stringArray[selectedCell!]
                }
            }
        }
    }

}

class collectionViewCell: UICollectionViewCell {
    @IBOutlet var label: UILabel!
    @IBOutlet var image: UIImageView!
}

